﻿var diagram_edit = new function initFullDiagram() {    

    this.drawNodeType = "";
    this.drawNodeKey = "";

    var $ = go.GraphObject.make;
    var myDiagram =
        $(go.Diagram, "ActivityDiagram",  // must name or refer to the DIV HTML element
            {
                grid: $(go.Panel, "Grid",
                    $(go.Shape, "LineH", { stroke: "#34495e", strokeWidth: 0.5, interval: 2 }),
                    $(go.Shape, "LineV", { stroke: "#34495e", strokeWidth: 0.5, interval: 2 }),
                    $(go.Shape, "LineH", { stroke: "#263238", strokeWidth: 0.5, interval: 1 }),
                    $(go.Shape, "LineV", { stroke: "#263238", strokeWidth: 0.5, interval: 1 })
                ),
                "grid.background": "#212121",
                "BackgroundDoubleClicked": function (e) {
                    DrawNewNode(e, currentDrawingMode);
                },
                "linkingTool.isUnconnectedLinkValid": true,
                "linkingTool.portGravity": 20,
                "relinkingTool.isUnconnectedLinkValid": true,
                "relinkingTool.portGravity": 20,
                "relinkingTool.fromHandleArchetype":
                $(go.Shape, "Diamond", { segmentIndex: 0, cursor: "pointer", desiredSize: new go.Size(8, 8), fill: "tomato", stroke: "darkred" }),
                "relinkingTool.toHandleArchetype":
                $(go.Shape, "Diamond", { segmentIndex: -1, cursor: "pointer", desiredSize: new go.Size(8, 8), fill: "darkred", stroke: "tomato" }),
                "linkReshapingTool.handleArchetype":
                $(go.Shape, "Diamond", { desiredSize: new go.Size(7, 7), fill: "lightblue", stroke: "deepskyblue" }),
                //rotatingTool: $(TopRotatingTool),  // defined below
                //"rotatingTool.snapAngleMultiple": 15,
                //"rotatingTool.snapAngleEpsilon": 15,
                "undoManager.isEnabled": true
            }
        );      

    myDiagram.nodeTemplate =
        $(go.Node, "Spot",
            $(go.Panel, "Auto",
                { name: "PANEL" },
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                $(go.Shape, "RoundedRectangle",  // default figure
                    {
                        portId: "", // the default port: if no spot on link data, use closest side
                        fromLinkable: true, toLinkable: true, cursor: "pointer",
                        fill: "white",  // default color
                        strokeWidth: 2
                    },
                    new go.Binding("figure"),
                    new go.Binding("fill")),
                $(go.TextBlock,
                    {
                        font: "bold 11pt Helvetica, Arial, sans-serif",
                        margin: 8,
                        wrap: go.TextBlock.WrapFit,
                        editable: true
                    },
                    new go.Binding("text", "key"))
            ),
            // four small named ports, one on each side:
            makePort("T", go.Spot.Top, false, true),
            makePort("L", go.Spot.Left, true, true),
            makePort("R", go.Spot.Right, true, true),
            makePort("B", go.Spot.Bottom, true, false),
            { // handle mouse enter/leave events to show/hide the ports
                mouseEnter: function (e, node) { showSmallPorts(node, true); },
                mouseLeave: function (e, node) { showSmallPorts(node, false); }
            }
        );

    function DrawNewNode(e, drawMode) {
        this.currentDrawingKey = GetCurrentDrawKey();
        this.currentDrawingMode = GetCurrentDrawMode();
        var x = e.diagram.lastInput.documentPoint;

        var names = this.currentDrawingKey.split(':');
        var figure = "";
        var link = {};
        var toNode = '';
        var fromNode = '';

        switch (currentDrawingMode) {
            case 'Activity':
                figure = "RoundedRectangle";
                break;
            case 'Role':
                figure = "Rectangle";
                break;
            case 'Alternative':
                figure = "Diamond";
                break;
            case 'Follow':
                figure = "RoundedRectangle";
                toNode = stack.pop();
                fromNode = stack.pop(); 
                break;
            case 'Precedes':
                figure = "RoundedRectangle";
                fromNode = stack.pop();
                toNode = stack.pop();                
                break;
        }               

        var nodeDataArray = [];        
        names.forEach(function (element) {
            nodeDataArray.push({
                key: element, figure: figure, id: element, fill: "#74b9ff" })
        }); 


        var arrayLength = nodeDataArray.length;
        for (var i = 0; i < arrayLength; i++) {
            if (myDiagram.findNodeForKey(nodeDataArray[i].key)) continue;
            myDiagram.model.addNodeData(nodeDataArray[i]);
            
        }
      
        if (nodeDataArray.length === 2) {
            var link1 = { from: fromNode, to: toNode };
            var modelJson = JSON.parse(myDiagram.model.toJson());
            var links = modelJson.linkDataArray;
            var exist = false;
            links.forEach(function (l) {
               if(l.from === link1.from && l.to === link1.to) exist = true;
            })
            if (!exist) {
                myDiagram.model.addLinkData(link1);
            }                             
        }
    }

    //this.drawDataDiagram = '';
    //this.LoadDrawData = function () {
    //    loadDataFromJson(this.drawDataDiagram);
    //    function loadDataFromJson(jsondata) {
    //        myDiagram.model = go.Model.fromJson(jsondata);
    //    }
    //}

       function saveDataToJson(jsondata) {
           this.drawDataDiagram = myDiagram.model.toJson();
       }
    
       //function loadDiagramProperties() {
       //    // set Diagram.initialPosition, not Diagram.position, to handle initialization side-effects
       //    var pos = myDiagram.model.modelData.position;
       //    if (pos) myDiagram.initialPosition = go.Point.parse(pos);
       //}

       myDiagram.model.addChangedListener(function (e) {
           if (e.isTransactionFinished) {
               saveDataToJson();
               var x = document.getElementById("DrawDataDiagramModel").textContent = this.drawDataDiagram;
           }
       });

        // Define a function for creating a "port" that is normally transparent.
        // The "name" is used as the GraphObject.portId, the "spot" is used to control how links connect
        // and where the port is positioned on the node, and the boolean "output" and "input" arguments
        // control whether the user can draw links from or to the port.
        function makePort(name, spot, output, input) {
            // the port is basically just a small transparent square
            return $(go.Shape, "Circle",
                {
                    fill: null,  // not seen, by default; set to a translucent gray by showSmallPorts, defined below
                    stroke: "#ecf0f1",
                    desiredSize: new go.Size(7, 7),
                    alignment: spot,  // align the port on the main Shape
                    alignmentFocus: spot,  // just inside the Shape
                    portId: name,  // declare this object to be a "port"
                    fromSpot: spot, toSpot: spot,  // declare where links may connect at this port
                    fromLinkable: output, toLinkable: input,  // declare whether the user may draw links to/from here
                    cursor: "pointer"  // show a different cursor to indicate potential link point
                });
       }

        var nodeSelectionAdornmentTemplate =
            $(go.Adornment, "Auto",
                $(go.Shape, { fill: null, stroke: "#ecf0f1", strokeWidth: 1.5, strokeDashArray: [4, 2] }),
                $(go.Placeholder)
            );

        var nodeResizeAdornmentTemplate =
            $(go.Adornment, "Spot",
                { locationSpot: go.Spot.Right },
                $(go.Placeholder),
                $(go.Shape, { alignment: go.Spot.TopLeft, cursor: "nw-resize", desiredSize: new go.Size(6, 6), fill: "#ecf0f1", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.Top, cursor: "n-resize", desiredSize: new go.Size(6, 6), fill: "#7f8c8d", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.TopRight, cursor: "ne-resize", desiredSize: new go.Size(6, 6), fill: "#ecf0f1", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.Left, cursor: "w-resize", desiredSize: new go.Size(6, 6), fill: "#7f8c8d", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.Right, cursor: "e-resize", desiredSize: new go.Size(6, 6), fill: "#7f8c8d", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.BottomLeft, cursor: "se-resize", desiredSize: new go.Size(6, 6), fill: "#ecf0f1", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.Bottom, cursor: "s-resize", desiredSize: new go.Size(6, 6), fill: "#7f8c8d", stroke: "deepskyblue" }),
                $(go.Shape, { alignment: go.Spot.BottomRight, cursor: "sw-resize", desiredSize: new go.Size(6, 6), fill: "#ecf0f1", stroke: "deepskyblue" })
            );

        var nodeRotateAdornmentTemplate =
            $(go.Adornment,
                { locationSpot: go.Spot.Center, locationObjectName: "CIRCLE" },
                $(go.Shape, "Circle", { name: "CIRCLE", cursor: "pointer", desiredSize: new go.Size(7, 7), fill: "lightblue", stroke: "deepskyblue" }),
                $(go.Shape, { geometryString: "M3.5 7 L3.5 30", isGeometryPositioned: true, stroke: "deepskyblue", strokeWidth: 1.5, strokeDashArray: [4, 2] })
            );

        function showSmallPorts(node, show) {
            node.ports.each(function (port) {
                if (port.portId !== "") {  // don't change the default port, which is the big shape
                    port.fill = show ? "rgba(0,0,0,.3)" : null;
                }
            });
        }

        var linkSelectionAdornmentTemplate =
            $(go.Adornment, "Link",
                $(go.Shape,
                    // isPanelMain declares that this Shape shares the Link.geometry
                    { isPanelMain: true, fill: null, stroke: "deepskyblue", strokeWidth: 0 })  // use selection object's strokeWidth
            );

        myDiagram.linkTemplate =
            $(go.Link,  // the whole link panel
                { selectable: true, selectionAdornmentTemplate: linkSelectionAdornmentTemplate },
                { relinkableFrom: true, relinkableTo: true, reshapable: true },
                {
                    routing: go.Link.AvoidsNodes,
                    curve: go.Link.JumpOver,
                    corner: 5,
                    toShortLength: 4
                },
                new go.Binding("points").makeTwoWay(),
                $(go.Shape,  // the link path shape
                    { isPanelMain: true, strokeWidth: 2, stroke: "#34495e" }),
                $(go.Shape,  // the arrowhead
                    { toArrow: "Standard", fill: "#ecf0f1", strokeWidth: 3, stroke: "#bdc3c7" }),
                $(go.Panel, "Auto",
                    new go.Binding("visible", "isSelected").ofObject(),
                    $(go.Shape, "RoundedRectangle",  // the link shape
                        { fill: "#F8F8F8", stroke: null }),
                    $(go.TextBlock,
                        {
                            textAlign: "center",
                            font: "11pt Microsoft JhengHei, Verdana",
                            stroke: "#bdc3c7",
                            margin: 2,
                            minSize: new go.Size(10, NaN),
                            editable: true
                        },
                        new go.Binding("text"))
                )
            );

        this.AddNewNode = function(){
            //alert(10);
        }

    //function TopRotatingTool() {
    //    go.RotatingTool.call(this);
    //}
    //go.Diagram.inherit(TopRotatingTool, go.RotatingTool);
    ///** @override */
    //TopRotatingTool.prototype.updateAdornments = function (part) {
    //    go.RotatingTool.prototype.updateAdornments.call(this, part);
    //    var adornment = part.findAdornment("Rotating");
    //    if (adornment !== null) {
    //        adornment.location = part.rotateObject.getDocumentPoint(new go.Spot(0.5, 0, 0, -30));  // above middle top
    //    }
    //};
    ///** @override */
    //TopRotatingTool.prototype.rotate = function (newangle) {
    //    go.RotatingTool.prototype.rotate.call(this, newangle + 90);
    //};
}