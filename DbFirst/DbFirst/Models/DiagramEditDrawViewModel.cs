﻿using DbFirst.Models.Northwind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UmlDiagramModels;

namespace DbFirst.Models
{
    public class DiagramEditDrawViewModel
    {
        public OwlDependenciesRules owlRules { get; set; }

        public UmlDiagram umlDiagram { get; set; }

        public DiagramEditDrawViewModel()
        {

        }

        public DiagramEditDrawViewModel(OwlDependenciesRules owlRules, UmlDiagram umlDiagram)
        {
            this.umlDiagram = umlDiagram;
            this.owlRules = owlRules;
        }
    }
}
