﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml;

namespace DbFirst.Models.Northwind
{
    public partial class OwlModel
    {
        public OwlModel()
        {
            UmlDiagram = new HashSet<UmlDiagram>();
        }

        public int ModelId { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "Model Name")]
        public string Name { get; set; }

        [Required]
        [XmlValidator]
        [Display(Name = "Model Data")]
        public string ModelData { get; set; }

        [Display(Name = "Created")]
        public DateTime? Created { get; set; }

        [Display(Name = "Last Modified")]
        public DateTime? LastModifed { get; set; }

        [Required]
        public string DefaultDrawData { get; set; }

        public ICollection<UmlDiagram> UmlDiagram { get; set; }
    }
}

public class XmlValidator : ValidationAttribute
{
    public XmlValidator()
    {
    }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        var errorMesage = string.Empty;

        try
        {
            var xmlString = value.ToString();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlString);
            return ValidationResult.Success;
        }
        catch
        {
            errorMesage = "Invalid OWL file.";
        }

        return new ValidationResult(errorMesage);
    }    
}
