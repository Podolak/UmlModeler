﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DbFirst.Models.Northwind
{
    public partial class UserAccount
    {
        public UserAccount()
        {
            UmlDiagram = new HashSet<UmlDiagram>();
        }

        public int UserId { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "User Name must have length between 5-20.")]
        [Display(Name = "User Name")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "User Password must have length between 5-20.")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        public bool? IsSuperUser { get; set; }

        [Display(Name = "Account Creation")]
        public DateTime? Created { get; set; }

        public DateTime? LastModifed { get; set; }

        public ICollection<UmlDiagram> UmlDiagram { get; set; }
    }
}
