﻿using System;
using System.Collections.Generic;

namespace DbFirst.Models.Northwind
{
    public partial class UmlDiagram
    {
        public int DiagramId { get; set; }
        public string Name { get; set; }
        public string DrawData { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? LastModifed { get; set; }
        public int? UserId { get; set; }
        public int? ModelId { get; set; }

        public OwlModel Model { get; set; }
        public UserAccount User { get; set; }
    }
}
