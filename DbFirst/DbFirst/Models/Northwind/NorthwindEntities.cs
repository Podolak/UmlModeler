﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DbFirst.Models.Northwind
{
    public partial class NorthwindEntities : DbContext
    {
        public virtual DbSet<OwlModel> OwlModel { get; set; }
        public virtual DbSet<UmlDiagram> UmlDiagram { get; set; }
        public virtual DbSet<UserAccount> UserAccount { get; set; }

       // private static readonly bool[] _migrated = { false };

        public NorthwindEntities(DbContextOptions<NorthwindEntities> options ) : base(options)
        {
            //if (!_migrated[0])
            //    lock (_migrated)
            //        if (!_migrated[0])
            //        {
            //            Database.Migrate(); // применение всех миграций
            //            _migrated[0] = true;
            //        }
        }
//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer(@"Data Source=3038-60172\TESTCASESQLDB;Initial Catalog=TestCase;User ID=sa;Password=29HROBarko12");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OwlModel>(entity =>
            {
                entity.HasKey(e => e.ModelId);

                entity.Property(e => e.ModelId).HasColumnName("ModelID");

                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifed)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModelData).HasDefaultValueSql("('{}')");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<UmlDiagram>(entity =>
            {
                entity.HasKey(e => e.DiagramId);

                entity.Property(e => e.DiagramId).HasColumnName("DiagramID");

                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DrawData).HasDefaultValueSql("('{}')");

                entity.Property(e => e.LastModifed)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModelId).HasColumnName("ModelID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.UmlDiagram)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__UmlDiagra__Model__45F365D3");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UmlDiagram)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__UmlDiagra__UserI__44FF419A");
            });

            modelBuilder.Entity<UserAccount>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.LastModifed)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            
        }
    }
}
