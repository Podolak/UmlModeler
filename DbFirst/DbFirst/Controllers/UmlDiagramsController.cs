﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DbFirst.Models.Northwind;
using System.Xml.Linq;
using DbFirst.Models;
using Newtonsoft.Json;
using UmlDiagramModels;
using System.IO;
using System.Net;

namespace DbFirst.Controllers
{
    public class UmlDiagramsController : Controller
    {
        private readonly NorthwindEntities _context;

        public UmlDiagramsController(NorthwindEntities context)
        {
            _context = context;
        }

        // GET: UmlDiagrams
        public async Task<IActionResult> Index()
        {
            var northwindEntities = _context.UmlDiagram.Include(u => u.Model).Include(u => u.User);
            return View(await northwindEntities.ToListAsync());
        }

        // GET: UmlDiagrams/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var umlDiagram = await _context.UmlDiagram
                .Include(u => u.Model)
                .Include(u => u.User)
                .SingleOrDefaultAsync(m => m.DiagramId == id);
            if (umlDiagram == null)
            {
                return NotFound();
            }

            return View(umlDiagram);
        }

        // GET: UmlDiagrams/Create
        public IActionResult Create()
        {
            ViewData["ModelId"] = new SelectList(_context.OwlModel, "ModelId", "Name");
            ViewData["UserId"] = new SelectList(_context.UserAccount, "UserId", "Name");
            return View();
        }

        // POST: UmlDiagrams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DiagramId,Name,DrawData,Created,LastModifed,UserId,ModelId")] UmlDiagram umlDiagram)
        {
            if (ModelState.IsValid)
            {
                _context.Add(umlDiagram);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ModelId"] = new SelectList(_context.OwlModel, "ModelId", "Name", umlDiagram.ModelId);
            ViewData["UserId"] = new SelectList(_context.UserAccount, "UserId", "Email", umlDiagram.UserId);
            return View(umlDiagram);
        }

        // GET: UmlDiagrams/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var umlDiagram = await _context.UmlDiagram.SingleOrDefaultAsync(m => m.DiagramId == id);
            if (umlDiagram == null)
            {
                return NotFound();
            }

            var rules = _context.OwlModel.Where(m => m.ModelId == umlDiagram.ModelId).Select(r => r.DefaultDrawData).FirstOrDefault();
            var owlRules = JsonConvert.DeserializeObject<OwlDependenciesRules>(rules);

            ViewData["ModelId"] = new SelectList(_context.OwlModel, "ModelId", "Name", umlDiagram.ModelId);
            ViewData["UserId"] = new SelectList(_context.UserAccount, "UserId", "Email", umlDiagram.UserId);

            var model = new DiagramEditDrawViewModel();
            model.owlRules = owlRules;
            model.umlDiagram = umlDiagram;

            return View(model);
        }

        // POST: UmlDiagrams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DiagramId,Name,DrawData,Created,LastModifed,UserId,ModelId")] UmlDiagram umlDiagram)
        {
            if (id != umlDiagram.DiagramId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {                
                try
                {
                    var modelBeforeEdit = _context.UmlDiagram.Where(m => m.DiagramId == umlDiagram.DiagramId).Select(nm => new { nm.Created, LastModifed=DateTime.Now, nm.Model.ModelId, nm.User.UserId }).FirstOrDefault();
                    umlDiagram.Created = modelBeforeEdit.Created;
                    umlDiagram.LastModifed = modelBeforeEdit.LastModifed;
                    umlDiagram.ModelId = modelBeforeEdit.ModelId;
                    umlDiagram.UserId = modelBeforeEdit.UserId;

                    _context.Update(umlDiagram);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UmlDiagramExists(umlDiagram.DiagramId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ModelId"] = new SelectList(_context.OwlModel, "ModelId", "Name", umlDiagram.ModelId);
            ViewData["UserId"] = new SelectList(_context.UserAccount, "UserId", "Email", umlDiagram.UserId);
            return View(umlDiagram);
        }

        // GET: UmlDiagrams/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var umlDiagram = await _context.UmlDiagram
                .Include(u => u.Model)
                .Include(u => u.User)
                .SingleOrDefaultAsync(m => m.DiagramId == id);
            if (umlDiagram == null)
            {
                return NotFound();
            }

            return View(umlDiagram);
        }

        // POST: UmlDiagrams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var umlDiagram = await _context.UmlDiagram.SingleOrDefaultAsync(m => m.DiagramId == id);
            _context.UmlDiagram.Remove(umlDiagram);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UmlDiagramExists(int id)
        {
            return _context.UmlDiagram.Any(e => e.DiagramId == id);
        }

        [HttpPost]
        public ActionResult mytry(DiagramEditDrawViewModel model)
        {
            var rules = _context.OwlModel.SingleOrDefault(o => o.ModelId == model.umlDiagram.ModelId).DefaultDrawData;
            model.owlRules = JsonConvert.DeserializeObject<OwlDependenciesRules>(rules);

            //var userName = _context.UserAccount.Where(u => u.UserId == model.umlDiagram.UserId).FirstOrDefault().Name;
            //var fileName = userName  + "_" + model.umlDiagram.Name + ".json";
            //string path = @"SavedData\" + fileName;
            //var logFile = System.IO.File.Create(path);
            //var logWriter = new System.IO.StreamWriter(logFile);
            //logWriter.WriteLine(model.umlDiagram.DrawData);
            //logWriter.Dispose();

            //string downloadsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            

            //using (WebClient webClient = new WebClient())
            //{
            //    webClient.DownloadFile(path, getDownloadFolderPath() + @"\myfile.json");
            //}
            //System.IO.File.Delete(path);

            var diagram = DiagramGenerator.DrawedDiagramGenerator.GenerateDiagram(model.umlDiagram.DrawData);
            var x = diagram.IsValid();


            return View();
        }

        public static string getHomePath()
        {
            // Not in .NET 2.0
            // System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            if (System.Environment.OSVersion.Platform == System.PlatformID.Unix)
                return System.Environment.GetEnvironmentVariable("HOME");

            return System.Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
        }

        public static string getDownloadFolderPath()
        {
            if (System.Environment.OSVersion.Platform == System.PlatformID.Unix)
            {
                string pathDownload = System.IO.Path.Combine(getHomePath(), "Downloads");
                return pathDownload;
            }

            return System.Convert.ToString(
                Microsoft.Win32.Registry.GetValue(
                     @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders"
                    , "{374DE290-123F-4565-9164-39C4925E467B}"
                    , String.Empty
                )
            );
        }
    }
}
