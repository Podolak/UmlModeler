﻿using DbFirst.Models;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Xml.Serialization;
using static DiagramMapper.PetriNetMapper;

namespace DbFirst.Controllers
{
    public class CompareController : Controller
    {
        public IActionResult Index()
        {
            var model = new PetriNetModel();
            return View(model);
        }

        public IActionResult Compare(PetriNetModel model)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(DiagramMapper.PetriNetMapper.PetriNet));
            using (TextReader reader = new StringReader(model.RawData))
            {
                PetriNet result = (PetriNet)serializer.Deserialize(reader);
            }
            return View(model);
        }
    }
}