﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DbFirst.Models.Northwind;
using System.Xml.Linq;
using System.Xml;
using DbFirst.Models;
using Newtonsoft.Json;

namespace DbFirst.Controllers
{
    public class OwlModelsController : Controller
    {
        private readonly NorthwindEntities _context;

        public OwlModelsController(NorthwindEntities context)
        {
            _context = context;
        }

        // GET: OwlModels
        public async Task<IActionResult> Index()
        {
            return View(await _context.OwlModel.ToListAsync());
        }

        // GET: OwlModels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var owlModel = await _context.OwlModel
                .SingleOrDefaultAsync(m => m.ModelId == id);
            if (owlModel == null)
            {
                return NotFound();
            }

            return View(owlModel);
        }

        // GET: OwlModels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: OwlModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ModelId,Name,ModelData,Created,LastModifed,DefaultDrawData")] OwlModel owlModel)
        {
            if (ModelState.IsValid)
            {            
                var names = _context.OwlModel.Select(m => m.Name.Trim().ToUpper()).ToList();
                if (names.Contains(owlModel.Name.Trim().ToUpper()))
                {
                    return View(owlModel);
                }

                XDocument xml = XDocument.Parse(owlModel.ModelData);
                XNamespace xmlNamespace = XNamespace.Get("http://www.w3.org/2002/07/owl#");

                var classAsertions = xml.Root.Descendants(xmlNamespace + "ClassAssertion").Descendants(xmlNamespace + "Class");
                var Activities = classAsertions.Where(c => c.LastAttribute.Value == "#Activity").Select(xp => xp.Parent).Descendants(xmlNamespace + "NamedIndividual").Select(ni => ni.LastAttribute.Value).ToList();
                var Roles = classAsertions.Where(c => c.LastAttribute.Value == "#Role").Select(xp => xp.Parent).Descendants(xmlNamespace + "NamedIndividual").Select(ni => ni.LastAttribute.Value).ToList();
                var Alternatives = classAsertions.Where(c => c.LastAttribute.Value == "#Alternative").Select(xp => xp.Parent).Descendants(xmlNamespace + "NamedIndividual").Select(ni => ni.LastAttribute.Value).ToList();

                var objectProperties = xml.Root.Elements(xmlNamespace + "ObjectPropertyAssertion").Elements(xmlNamespace + "ObjectProperty").ToList();
                var Follows = OwlModelDoubleDependencies(DetectDoubleDependenciesFromObjectPropery(objectProperties, xmlNamespace, "#Follows"));
                var Precedes = OwlModelDoubleDependencies(DetectDoubleDependenciesFromObjectPropery(objectProperties, xmlNamespace, "#Precedes"));
                var Performs = OwlModelDoubleDependencies(DetectDoubleDependenciesFromObjectPropery(objectProperties, xmlNamespace, "#Performs")).ToList();
                var Decides = OwlModelDoubleDependencies(DetectDoubleDependenciesFromObjectPropery(objectProperties, xmlNamespace, "#Decides"));
                var IsFollowedBy = OwlModelDoubleDependencies(DetectDoubleDependenciesFromObjectPropery(objectProperties, xmlNamespace, "#IsFollowedBy"));
                var IsPrecededBy = OwlModelDoubleDependencies(DetectDoubleDependenciesFromObjectPropery(objectProperties, xmlNamespace, "#IsPrecededBy"));
                var IsPerformed = OwlModelDoubleDependencies(DetectDoubleDependenciesFromObjectPropery(objectProperties, xmlNamespace, "#IsPerformed"));
                var IsDecidedBy = OwlModelDoubleDependencies(DetectDoubleDependenciesFromObjectPropery(objectProperties, xmlNamespace, "#IsDecidedBy"));

                //OwlDependenciesRules rules = new OwlDependenciesRules(Activities, Roles, Alternatives, Follows, Precedes, Decides, IsFollowedBy, IsPrecededBy, IsDecidedBy);
                //owlModel.DefaultDrawData = rules.ToJson();

                _context.Add(owlModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(owlModel);
        }

        // GET: OwlModels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var owlModel = await _context.OwlModel.SingleOrDefaultAsync(m => m.ModelId == id);
            if (owlModel == null)
            {
                return NotFound();
            }
            return View(owlModel);
        }

        // POST: OwlModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ModelId,Name,ModelData,Created,LastModifed,DefaultDrawData")] OwlModel owlModel)
        {
            if (id != owlModel.ModelId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var modelBeforeUpdate = _context.OwlModel.Where(m => m.ModelId == owlModel.ModelId).Select(nm => new { Created = nm.Created, LastModifed = DateTime.Now, ModelData = nm.ModelData }).FirstOrDefault();
                    owlModel.ModelData = modelBeforeUpdate.ModelData;
                    owlModel.Created = modelBeforeUpdate.Created;
                    owlModel.LastModifed = modelBeforeUpdate.LastModifed;

                    _context.Update(owlModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OwlModelExists(owlModel.ModelId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(owlModel);
        }

        // GET: OwlModels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var owlModel = await _context.OwlModel
                .SingleOrDefaultAsync(m => m.ModelId == id);
            if (owlModel == null)
            {
                return NotFound();
            }

            return View(owlModel);
        }

        // POST: OwlModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var owlModel = await _context.OwlModel.SingleOrDefaultAsync(m => m.ModelId == id);
            _context.OwlModel.Remove(owlModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OwlModelExists(int id)
        {
            return _context.OwlModel.Any(e => e.ModelId == id);
        }


        private List<string> DetectDoubleDependencies(XDocument xml, XNamespace xmlNamespace, string dependence)
        {
            return xml.Root.Elements(xmlNamespace + "ObjectPropertyAssertion").Elements(xmlNamespace + "ObjectProperty").Where(c => c.LastAttribute.Value == dependence).Select(xp => xp.Parent).Elements(xmlNamespace + "NamedIndividual").Select(ni => ni.LastAttribute.Value).ToList();
        }

        private List<string> DetectDoubleDependenciesFromObjectPropery(List<XElement> xml, XNamespace xmlNamespace, string dependence)
        {
            return xml.Where(c => c.LastAttribute.Value == dependence).Select(xp => xp.Parent).Elements(xmlNamespace + "NamedIndividual").Select(ni => ni.LastAttribute.Value).ToList();
        }

        private List<string> OwlModelDoubleDependencies(List<string> properties)
        {
            List<string> doubleProperties = new List<string>();
            if (properties.Count % 2 == 0)
            {
                for (int i = 0; i < properties.Count; i++)
                {
                    doubleProperties.Add(string.Format("[{0}]:[{1}]", properties[i], properties[++i]));
                }
            }
            return doubleProperties;
        }


    }
}
