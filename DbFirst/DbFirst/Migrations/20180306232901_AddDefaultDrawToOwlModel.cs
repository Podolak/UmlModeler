﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DbFirst.Migrations
{
    public partial class AddDefaultDrawToOwlModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OwlModel",
                columns: table => new
                {
                    ModelID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    DefaultDrawData = table.Column<string>(nullable: false),
                    LastModifed = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    ModelData = table.Column<string>(nullable: false, defaultValueSql: "('{}')"),
                    Name = table.Column<string>(unicode: false, maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OwlModel", x => x.ModelID);
                });

            migrationBuilder.CreateTable(
                name: "UserAccount",
                columns: table => new
                {
                    UserID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    Email = table.Column<string>(unicode: false, maxLength: 25, nullable: false),
                    IsSuperUser = table.Column<bool>(nullable: true),
                    LastModifed = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    Name = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Password = table.Column<string>(unicode: false, maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAccount", x => x.UserID);
                });

            migrationBuilder.CreateTable(
                name: "UmlDiagram",
                columns: table => new
                {
                    DiagramID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    DrawData = table.Column<string>(nullable: true, defaultValueSql: "('{}')"),
                    LastModifed = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    ModelID = table.Column<int>(nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    UserID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UmlDiagram", x => x.DiagramID);
                    table.ForeignKey(
                        name: "FK__UmlDiagra__Model__45F365D3",
                        column: x => x.ModelID,
                        principalTable: "OwlModel",
                        principalColumn: "ModelID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__UmlDiagra__UserI__44FF419A",
                        column: x => x.UserID,
                        principalTable: "UserAccount",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UmlDiagram_ModelID",
                table: "UmlDiagram",
                column: "ModelID");

            migrationBuilder.CreateIndex(
                name: "IX_UmlDiagram_UserID",
                table: "UmlDiagram",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UmlDiagram");

            migrationBuilder.DropTable(
                name: "OwlModel");

            migrationBuilder.DropTable(
                name: "UserAccount");
        }
    }
}
