﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace DiagramMapper
{
    class OntologyMapper
    {
        [XmlRoot(ElementName = "Ontology", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class Ontology
        {
            [XmlElement(ElementName = "Prefix", Namespace = "http://www.w3.org/2002/07/owl#")]
            public List<Prefix> Prefix { get; set; }

            [XmlElement(ElementName = "Declaration", Namespace = "http://www.w3.org/2002/07/owl#")]
            public List<Declaration> Declaration { get; set; }

            [XmlElement(ElementName = "EquivalentClasses", Namespace = "http://www.w3.org/2002/07/owl#")]
            public EquivalentClasses EquivalentClasses { get; set; }

            [XmlElement(ElementName = "ClassAssertion", Namespace = "http://www.w3.org/2002/07/owl#")]
            public List<ClassAssertion> ClassAssertion { get; set; }

            [XmlElement(ElementName = "ObjectPropertyAssertion", Namespace = "http://www.w3.org/2002/07/owl#")]
            public List<ObjectPropertyAssertion> ObjectPropertyAssertion { get; set; }

            [XmlElement(ElementName = "SubObjectPropertyOf", Namespace = "http://www.w3.org/2002/07/owl#")]
            public List<SubObjectPropertyOf> SubObjectPropertyOf { get; set; }

            [XmlElement(ElementName = "InverseObjectProperties", Namespace = "http://www.w3.org/2002/07/owl#")]
            public List<InverseObjectProperties> InverseObjectProperties { get; set; }

            [XmlElement(ElementName = "ObjectPropertyDomain", Namespace = "http://www.w3.org/2002/07/owl#")]
            public List<ObjectPropertyDomain> ObjectPropertyDomain { get; set; }

            [XmlElement(ElementName = "ObjectPropertyRange", Namespace = "http://www.w3.org/2002/07/owl#")]
            public List<ObjectPropertyRange> ObjectPropertyRange { get; set; }

            [XmlAttribute(AttributeName = "xmlns")]
            public string Xmlns { get; set; }

            [XmlAttribute(AttributeName = "base", Namespace = "http://www.w3.org/XML/1998/namespace")]
            public string Base { get; set; }

            [XmlAttribute(AttributeName = "rdf", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Rdf { get; set; }

            [XmlAttribute(AttributeName = "xml", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Xml { get; set; }

            [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Xsd { get; set; }

            [XmlAttribute(AttributeName = "rdfs", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Rdfs { get; set; }

            [XmlAttribute(AttributeName = "ontologyIRI")]
            public string OntologyIRI { get; set; }
        }


        [XmlRoot(ElementName = "Prefix", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class Prefix
        {
            [XmlAttribute(AttributeName = "name")]
            public string Name { get; set; }

            [XmlAttribute(AttributeName = "IRI")]
            public string IRI { get; set; }
        }


        [XmlRoot(ElementName = "Class", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class Class
        {
            [XmlAttribute(AttributeName = "IRI")]
            public string IRI { get; set; }
        }


        [XmlRoot(ElementName = "Declaration", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class Declaration
        {
            [XmlElement(ElementName = "Class", Namespace = "http://www.w3.org/2002/07/owl#")]
            public Class Class { get; set; }

            [XmlElement(ElementName = "ObjectProperty", Namespace = "http://www.w3.org/2002/07/owl#")]
            public ObjectProperty ObjectProperty { get; set; }

            [XmlElement(ElementName = "NamedIndividual", Namespace = "http://www.w3.org/2002/07/owl#")]
            public NamedIndividual NamedIndividual { get; set; }
        }


        [XmlRoot(ElementName = "ObjectProperty", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class ObjectProperty
        {
            [XmlAttribute(AttributeName = "IRI")]
            public string IRI { get; set; }

            [XmlAttribute(AttributeName = "abbreviatedIRI")]
            public string AbbreviatedIRI { get; set; }
        }


        [XmlRoot(ElementName = "NamedIndividual", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class NamedIndividual
        {
            [XmlAttribute(AttributeName = "IRI")]
            public string IRI { get; set; }
        }


        [XmlRoot(ElementName = "EquivalentClasses", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class EquivalentClasses
        {
            [XmlElement(ElementName = "Class", Namespace = "http://www.w3.org/2002/07/owl#")]
            public List<Class> Class { get; set; }
        }


        [XmlRoot(ElementName = "ClassAssertion", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class ClassAssertion
        {
            [XmlElement(ElementName = "Class", Namespace = "http://www.w3.org/2002/07/owl#")]
            public Class Class { get; set; }

            [XmlElement(ElementName = "NamedIndividual", Namespace = "http://www.w3.org/2002/07/owl#")]
            public NamedIndividual NamedIndividual { get; set; }
        }

        [XmlRoot(ElementName = "ObjectPropertyAssertion", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class ObjectPropertyAssertion
        {
            [XmlElement(ElementName = "ObjectProperty", Namespace = "http://www.w3.org/2002/07/owl#")]
            public ObjectProperty ObjectProperty { get; set; }

            [XmlElement(ElementName = "NamedIndividual", Namespace = "http://www.w3.org/2002/07/owl#")]
            public List<NamedIndividual> NamedIndividual { get; set; }
        }

        [XmlRoot(ElementName = "SubObjectPropertyOf", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class SubObjectPropertyOf
        {
            [XmlElement(ElementName = "ObjectProperty", Namespace = "http://www.w3.org/2002/07/owl#")]
            public List<ObjectProperty> ObjectProperty { get; set; }
        }

        [XmlRoot(ElementName = "InverseObjectProperties", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class InverseObjectProperties
        {
            [XmlElement(ElementName = "ObjectProperty", Namespace = "http://www.w3.org/2002/07/owl#")]
            public List<ObjectProperty> ObjectProperty { get; set; }
        }

        [XmlRoot(ElementName = "ObjectPropertyDomain", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class ObjectPropertyDomain
        {
            [XmlElement(ElementName = "ObjectProperty", Namespace = "http://www.w3.org/2002/07/owl#")]
            public ObjectProperty ObjectProperty { get; set; }
            [XmlElement(ElementName = "Class", Namespace = "http://www.w3.org/2002/07/owl#")]
            public Class Class { get; set; }
        }

        [XmlRoot(ElementName = "ObjectPropertyRange", Namespace = "http://www.w3.org/2002/07/owl#")]
        public class ObjectPropertyRange
        {
            [XmlElement(ElementName = "ObjectProperty", Namespace = "http://www.w3.org/2002/07/owl#")]
            public ObjectProperty ObjectProperty { get; set; }
            [XmlElement(ElementName = "Class", Namespace = "http://www.w3.org/2002/07/owl#")]
            public Class Class { get; set; }
        }       

    }
}
