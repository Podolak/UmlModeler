﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DiagramMapper
{
    public class PetriNetMapper
    {

        [XmlRoot(ElementName = "name")]
        public class Name
        {
            [XmlElement(ElementName = "text")]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "toolspecific")]
        public class Toolspecific
        {
            [XmlAttribute(AttributeName = "localNodeID")]
            public string LocalNodeID { get; set; }
            [XmlAttribute(AttributeName = "tool")]
            public string Tool { get; set; }
            [XmlAttribute(AttributeName = "version")]
            public string Version { get; set; }
            [XmlAttribute(AttributeName = "activity")]
            public string Activity { get; set; }
        }

        [XmlRoot(ElementName = "position")]
        public class Position
        {
            [XmlAttribute(AttributeName = "x")]
            public string X { get; set; }
            [XmlAttribute(AttributeName = "y")]
            public string Y { get; set; }
        }

        [XmlRoot(ElementName = "dimension")]
        public class Dimension
        {
            [XmlAttribute(AttributeName = "x")]
            public string X { get; set; }
            [XmlAttribute(AttributeName = "y")]
            public string Y { get; set; }
        }

        [XmlRoot(ElementName = "graphics")]
        public class Graphics
        {
            [XmlElement(ElementName = "position")]
            public Position Position { get; set; }
            [XmlElement(ElementName = "dimension")]
            public Dimension Dimension { get; set; }
            [XmlElement(ElementName = "fill")]
            public Fill Fill { get; set; }
        }

        [XmlRoot(ElementName = "initialMarking")]
        public class InitialMarking
        {
            [XmlElement(ElementName = "text")]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "place")]
        public class Place
        {
            [XmlElement(ElementName = "name")]
            public Name Name { get; set; }
            [XmlElement(ElementName = "toolspecific")]
            public Toolspecific Toolspecific { get; set; }
            [XmlElement(ElementName = "graphics")]
            public Graphics Graphics { get; set; }
            [XmlElement(ElementName = "initialMarking")]
            public InitialMarking InitialMarking { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
            [XmlElement(ElementName = "finalMarking")]
            public FinalMarking FinalMarking { get; set; }
            [XmlElement(ElementName = "text")]
            public string Text { get; set; }
            [XmlAttribute(AttributeName = "idref")]
            public string Idref { get; set; }
        }

        [XmlRoot(ElementName = "finalMarking")]
        public class FinalMarking
        {
            [XmlElement(ElementName = "text")]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "fill")]
        public class Fill
        {
            [XmlAttribute(AttributeName = "color")]
            public string Color { get; set; }
        }

        [XmlRoot(ElementName = "transition")]
        public class Transition
        {
            [XmlElement(ElementName = "name")]
            public Name Name { get; set; }
            [XmlElement(ElementName = "toolspecific")]
            public Toolspecific Toolspecific { get; set; }
            [XmlElement(ElementName = "graphics")]
            public Graphics Graphics { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
            [XmlAttribute(AttributeName = "guard")]
            public string Guard { get; set; }
            [XmlAttribute(AttributeName = "invisible")]
            public string Invisible { get; set; }
        }

        [XmlRoot(ElementName = "arctype")]
        public class Arctype
        {
            [XmlElement(ElementName = "text")]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "arc")]
        public class Arc
        {
            [XmlElement(ElementName = "name")]
            public Name Name { get; set; }
            [XmlElement(ElementName = "toolspecific")]
            public Toolspecific Toolspecific { get; set; }
            [XmlElement(ElementName = "arctype")]
            public Arctype Arctype { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
            [XmlAttribute(AttributeName = "source")]
            public string Source { get; set; }
            [XmlAttribute(AttributeName = "target")]
            public string Target { get; set; }
        }

        [XmlRoot(ElementName = "page")]
        public class Page
        {
            [XmlElement(ElementName = "name")]
            public Name Name { get; set; }
            [XmlElement(ElementName = "place")]
            public List<Place> Place { get; set; }
            [XmlElement(ElementName = "transition")]
            public List<Transition> Transition { get; set; }
            [XmlElement(ElementName = "arc")]
            public List<Arc> Arc { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
        }

        [XmlRoot(ElementName = "marking")]
        public class Marking
        {
            [XmlElement(ElementName = "place")]
            public List<Place> Place { get; set; }
        }

        [XmlRoot(ElementName = "finalmarkings")]
        public class Finalmarkings
        {
            [XmlElement(ElementName = "marking")]
            public Marking Marking { get; set; }
        }

        [XmlRoot(ElementName = "net")]
        public class Net
        {
            [XmlElement(ElementName = "name")]
            public Name Name { get; set; }
            [XmlElement(ElementName = "page")]
            public Page Page { get; set; }
            [XmlElement(ElementName = "finalmarkings")]
            public Finalmarkings Finalmarkings { get; set; }
            [XmlElement(ElementName = "variables")]
            public string Variables { get; set; }
            [XmlAttribute(AttributeName = "id")]
            public string Id { get; set; }
            [XmlAttribute(AttributeName = "type")]
            public string Type { get; set; }
        }

        [XmlRoot(ElementName = "pnml")]
        public class PetriNet //IN PNML FORMAT
        {
            [XmlElement(ElementName = "net")]
            public Net Net { get; set; }
        }

    }
}
