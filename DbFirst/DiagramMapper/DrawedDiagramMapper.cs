﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiagramMapper
{
    public class DrawedDiagramMapper
    {
        public class ModelData
        {
            public string position { get; set; }
        }

        public class NodeDataArray
        {
            public string text { get; set; }
            public string figure { get; set; }
            public object fill { get; set; }
            public string stroke { get; set; }
            public int key { get; set; }
            public string loc { get; set; }
            public int? margin { get; set; }
        }

        public class LinkDataArray
        {
            public int from { get; set; }
            public int to { get; set; }
            public List<double> points { get; set; }
        }

        public class RootObject
        {
            public string @class { get; set; }
            public ModelData modelData { get; set; }
            public List<NodeDataArray> nodeDataArray { get; set; }
            public List<LinkDataArray> linkDataArray { get; set; }
        }
    }
}
