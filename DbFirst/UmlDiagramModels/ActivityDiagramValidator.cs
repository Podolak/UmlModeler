﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UmlDiagramModels
{
    public static class ActivityDiagramValidator
    {
        const int countStart = 1;
        const int minimalCountEnd = 1;
        const int maximalActivityToLinks = 1;
        const int minimalActivityFromLinks = 1;

        
        public static bool IsValid(Diagram diagram)
        {           
            if(diagram.Nodes.Where(n => n.Type == EDiagramNodeType.StartNode).Count() != countStart)
            {
                return false;
            }

            var start = diagram.Nodes.Where(n => n.Type == EDiagramNodeType.StartNode).FirstOrDefault();
            if(start.PreviousNode.Count != 0 || start.NextNode.Count == 0)
            {
                return false;
            }

            var ends = diagram.Nodes.Where(n => n.Type == EDiagramNodeType.EndNode).ToList();
            if(ends.Count < minimalCountEnd)
            {
                return false;
            }

            foreach(var e in ends)
            {
                if (e.NextNode.Count > 0)
                {
                    return false;
                }

                if (e.PreviousNode.Count < minimalActivityFromLinks)
                {
                    return false;
                }
            }

            foreach (var n in diagram.Nodes.Where(n => (n.Type != EDiagramNodeType.EndNode && n.Type != EDiagramNodeType.StartNode)))
            {
                if(n.NextNode.Count < maximalActivityToLinks || String.IsNullOrEmpty(n.Name.Trim()))
                {
                    return false;
                }
                if(n.Type != EDiagramNodeType.Role)
                {
                    if(n.PreviousNode.Count < 1)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
