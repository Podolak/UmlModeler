﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace UmlDiagramModels
{
    #region Diagram classes

    public class Diagram
    {
        public int ID { get; set; }

        public EDiagramType Type { get; set; }

        public String Name { get; set; }

        public List<DiagramNode> Nodes { get; set; }

        public OwlDependenciesRules OwlRules { get; set; }

        public Diagram()
        {
            ID = -1;
            Name = string.Empty;
            Type = EDiagramType.Undefined;
            Nodes = new List<DiagramNode>();
        }

        public Diagram(int id, string name, EDiagramType type, List<DiagramNode> nodes, OwlDependenciesRules owlRules)
        {
            ID = id;
            Name = name;
            Type = type;
            Nodes = nodes;
            OwlRules = owlRules;
        }

        public bool IsValid()
        {
            return ActivityDiagramValidator.IsValid(this);
        }

        public bool GetPaths()
        {
            var paths = Nodes.Select(x => new PathNode(x.ID)).ToList();
            var node = Nodes.Where(x => x.Type == EDiagramNodeType.StartNode).FirstOrDefault();

            Dictionary<int, string> divideSetup = new Dictionary<int, string>();
            foreach(var d in Nodes.Where(x => x.Type == EDiagramNodeType.DecideNode).ToList())
            {
                divideSetup.Add(d.ID, String.Format("{0}:{1}", -1, d.NextNode.Count));
            }

            Stack<DiagramNode> stack = new Stack<DiagramNode>();
            var p = paths.Where(x => x.ID == node.ID).FirstOrDefault();
            p.IsVisited = true;
            stack.Push(node);

            do
            {
                node = node.NextNode.FirstOrDefault().NodeTo;
                p = paths.Where(x => x.ID == node.ID).FirstOrDefault();
                while (p.IsVisited && node.Type!=EDiagramNodeType.DecideNode)
                {
                    if(node.Type == EDiagramNodeType.EndNode)
                    {
                        //add path to paths
                    }
                    stack.Pop();
                    node = stack.Peek();
                    p = paths.Where(x => x.ID == node.ID).FirstOrDefault();
                }    

                if(node.Type == EDiagramNodeType.DecideNode)
                {
                    var setup = divideSetup[node.ID].Split(':');
                    var currentLink = Convert.ToInt32(setup.FirstOrDefault());
                    var countLink = Convert.ToInt32(setup.LastOrDefault());
                    divideSetup[node.ID] = String.Format("{0}:{1}", currentLink + 1, countLink);
                }

                p.IsVisited = true;
                stack.Push(node);

            } while (stack.Count > 0);

            return true;
        }
    }    

    public class DiagramNode
    {
        public int ID { get; set; }

        public String Name { get; set; }

        public List<LinkNode> PreviousNode { get; set; }

        public List<LinkNode> NextNode { get; set; }

        public EDiagramNodeType Type { get; set; }

        public DiagramNode()
        {
            ID = -1;
            Name = string.Empty;
            PreviousNode = new List<LinkNode>();
            NextNode = new List<LinkNode>();
            Type = EDiagramNodeType.Undefined;
        }

        public DiagramNode(int id, string name, List<LinkNode> previousNode, List<LinkNode> nextNode, EDiagramNodeType type)
        {
            ID = id;
            Name = name;
            PreviousNode = previousNode;
            NextNode = nextNode;
            Type = type;
        }

        public DiagramNode(int id, string name, EDiagramNodeType type)
        {
            ID = id;
            Name = name;
            PreviousNode = new List<LinkNode>();
            NextNode = new List<LinkNode>();
            Type = type;
        }
    }       

    public class LinkNode
    {
        public DiagramNode NodeFrom { get; set; }

        public DiagramNode NodeTo { get; set; }

        public string Title { get; set; }

        public const EDiagramNodeType Type = EDiagramNodeType.Link;

        //  public EDiagramLinkType LinkType { get; set; }

        public LinkNode()
        {
            NodeFrom = null;
            NodeTo = null;
            Title = String.Empty;
            // LinkType = EDiagramLinkType.Undefined;
        }

        public LinkNode(DiagramNode From, DiagramNode To, string title)
        {
            NodeFrom = From;
            NodeTo = To;
            Title = title;
            //   LinkType = linkType;
        }
    }


    public class PathNode
    {
        public int ID { get; set; }
        public bool IsVisited { get; set; }

        public PathNode()
        {
            IsVisited = false;
        }

        public PathNode(int id)
        {
            ID = id;
            IsVisited = false;
        }
    }
    #endregion

    #region Diagram Enums
    public enum EDiagramNodeType
    {
        Activity,
        DecideNode,
        Link,
        StartNode,
        EndNode,
        Role,
        Undefined
    }

    public enum EDiagramType
    {
        ActivityDiagram,
        //SequenceDiagram,
        //StateDiagram,
        //ClassDiagram
        Undefined
    }
    #endregion
}