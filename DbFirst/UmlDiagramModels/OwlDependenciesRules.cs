﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace UmlDiagramModels
{
    [DataContract]
    public class OwlDependenciesRules
    {
        [DataMember]
        public List<string> Activities { get; set; }

        [DataMember]
        public List<string> Roles { get; set; }

        [DataMember]
        public List<string> Alternatives { get; set; }

        [DataMember]
        public List<string> Follows { get; set; }

        [DataMember]
        public List<string> Precedes { get; set; }

        [DataMember]
        public List<string> Decides { get; set; }

        [DataMember]
        public List<string> IsFollowedBy { get; set; }

        [DataMember]
        public List<string> IsPrecededBy { get; set; }

        [DataMember]
        public List<string> IsDecidedBy { get; set; }


        public OwlDependenciesRules()
        {

        }

        public OwlDependenciesRules(List<string> activities, List<string> roles, List<string> alternatives, List<string> follows, List<string> precedes, List<string> decides, List<string> isFollowedBy, List<string> isPrecededBy, List<string> isDecidedBy)
        {
            Activities = activities;
            Roles = roles;
            Alternatives = alternatives;
            Follows = follows;
            Precedes = precedes;
            Decides = decides;
            IsFollowedBy = isFollowedBy;
            IsPrecededBy = isPrecededBy;
            IsDecidedBy = isDecidedBy;
        }

        public string ToJson()
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(OwlDependenciesRules));

            var obj = new OwlDependenciesRules
            {
                Activities = this.Activities,
                Roles = this.Roles,
                Alternatives = this.Alternatives,
                Follows = this.Follows,
                Precedes = this.Precedes,
                Decides = this.IsDecidedBy,
                IsFollowedBy = this.IsFollowedBy,
                IsPrecededBy = this.IsPrecededBy,
                IsDecidedBy = this.IsDecidedBy
            };

            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, obj);
            byte[] json = ms.ToArray();
            ms.Close();
            return Encoding.UTF8.GetString(json, 0, json.Length);
        }
    }
}
