﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UmlDiagramModels;

namespace DiagramGenerator
{
    public static class DrawedDiagramGenerator
    {
        public static Diagram GenerateDiagram(string data)
        {
            var drawedDiagram = JsonConvert.DeserializeObject<DiagramMapper.DrawedDiagramMapper.RootObject>(data);

            var nodes = new List<DiagramNode>();

            foreach (var node in drawedDiagram.nodeDataArray)
            {
                EDiagramNodeType type = EDiagramNodeType.Undefined;
                switch (node.figure)
                {
                    case "RoundedRectangle":
                        type = EDiagramNodeType.Activity;
                        break;
                    case "Circle":
                        type = EDiagramNodeType.EndNode;
                        break;
                    case "Diamond":
                        type = EDiagramNodeType.DecideNode;
                        break;
                    case "Rectangle":
                        type = EDiagramNodeType.Role;
                        break;
                    default:
                        break;
                }
                nodes.Add(new DiagramNode(node.key, node.text, type));
            }

            nodes.Where(n => n.Type == EDiagramNodeType.EndNode).FirstOrDefault().Type = EDiagramNodeType.StartNode;


            foreach (var link in drawedDiagram.linkDataArray)
            {
                var fromNode = nodes.Where(n => n.ID == link.from).FirstOrDefault();
                var toNode = nodes.Where(n => n.ID == link.to).FirstOrDefault();
                var linkNode = new LinkNode(fromNode, toNode, string.Empty);

                fromNode.NextNode.Add(linkNode);
                toNode.PreviousNode.Add(linkNode);

            }

            return new Diagram(1, "Undefined", EDiagramType.ActivityDiagram, nodes, null);
        }
    }
}
